//
//  LabelController.swift
//  Cards
//
//  Created by Nikita on 18.08.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit
import IGListKit

final class LabelController: ListSectionController {
    
    private var text: String?
    
    override func sizeForItem(at index: Int) -> CGSize {
        return CGSize(width: collectionContext!.containerSize.width, height: 30)
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell = collectionContext?.dequeueReusableCell(of: LabelCell.self, for: self, at: index) as? LabelCell else {
            fatalError("cell must be 'LabelCell'")
        }
        cell.text = text
        return cell
    }
    
    override func didUpdate(to object: Any) {
        self.text = object as? String
    }
    
}

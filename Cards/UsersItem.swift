//
//  UsersItem.swift
//  Cards
//
//  Created by Nikita on 18.08.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit
import IGListKit

final class UsersItem: ListDiffable {
    let items:[UserItem]
    private(set) var isExtended:Bool
    
    init(items: [UserItem], extended: Bool = false) {
        self.items = items
        self.isExtended = extended
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return items as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? UsersItem else { return false }
        if items.count != object.items.count {
            return false
        }
        for i in 0..<items.count {
            let item = items[i]
            if !item.isEqual(toDiffableObject: object.items[i]) {
                return false
            }
        }
        return true
    }
}

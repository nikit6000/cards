//
//  CircleButtonView.swift
//  Cards
//
//  Created by Nikita on 18.08.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit

final class CircleButtonCell: UICollectionViewCell {
    
    lazy private var emojiContainer: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(view)
        return view
    }()
    
    lazy private var emojiLabel: UILabel = {
        let view = UILabel()
        view.backgroundColor = .clear
        view.textAlignment = .center
        view.font = .systemFont(ofSize: 18)
        view.translatesAutoresizingMaskIntoConstraints = false
        self.emojiContainer.addSubview(view)
        return view
    }()
    
    lazy private var titleLabel: UILabel = {
        let view = UILabel()
        view.backgroundColor = .clear
        view.textAlignment = .center
        view.textColor = .white
        view.font = .systemFont(ofSize: 10)
        view.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(view)
        return view
    }()
    
    
    public var color:UIColor? {
        get{
            return emojiContainer.backgroundColor
        }
        set(newColor){
            emojiContainer.backgroundColor = newColor
        }
    }
    
    public var emoji:String? {
        get{
            return emojiLabel.text
        }
        set(newEmoji){
            emojiLabel.text = newEmoji
        }
    }
    
    public var title:String? {
        get{
            return titleLabel.text
        }
        set(newTitle){
            titleLabel.text = newTitle
        }
    }
    
    public var titleColor: UIColor {
        get{
            return titleLabel.textColor
        }
        set(newColor){
            titleLabel.textColor = newColor
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        emojiContainer.layoutIfNeeded()
        emojiContainer.layer.cornerRadius = emojiContainer.frame.height / 2.0
    }
    
    
    
    // MARK: setting up AutoLayout
    
    private func setupConstraints(){
        
        // emojiContainer
        
        do {
            let centerY = NSLayoutConstraint(item: emojiContainer, attribute: .centerY, relatedBy: .equal, toItem: contentView, attribute: .centerY, multiplier: 1.0, constant: 0.0)
            let height = NSLayoutConstraint(item: emojiContainer, attribute: .height, relatedBy: .equal, toItem: emojiContainer, attribute: .width, multiplier: 1.0, constant: 0.0)
            let width = NSLayoutConstraint(item: emojiContainer, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50.0)
            let centerX = NSLayoutConstraint(item: emojiContainer, attribute: .centerX, relatedBy: .equal, toItem: contentView, attribute: .centerX, multiplier: 1.0, constant: 0.0)
            
            contentView.addConstraints([centerY, centerX])
            emojiContainer.addConstraints([width, height])
        }
        
        // emojiLabel
        
        do {
            let centerX = NSLayoutConstraint(item: emojiLabel, attribute: .centerX, relatedBy: .equal, toItem: emojiContainer, attribute: .centerX, multiplier: 1.0, constant: 0.0)
            let centerY = NSLayoutConstraint(item: emojiLabel, attribute: .centerY, relatedBy: .equal, toItem: emojiContainer, attribute: .centerY, multiplier: 1.0, constant: 0.0)
            emojiContainer.addConstraints([centerX, centerY])
        }
        
        // titleLabel
        
        do {
            let leading = NSLayoutConstraint(item: titleLabel, attribute: .leading, relatedBy: .equal, toItem: contentView, attribute: .leading, multiplier: 1.0, constant: 0.0)
            let trailing = NSLayoutConstraint(item: contentView, attribute: .trailing, relatedBy: .equal, toItem: titleLabel, attribute: .trailing, multiplier: 1.0, constant: 0.0)
            let bottom = NSLayoutConstraint(item: contentView, attribute: .bottom, relatedBy: .equal, toItem: titleLabel, attribute: .bottom, multiplier: 1.0, constant: 8.0)
            contentView.addConstraints([leading, trailing, bottom])
        }
        
        NSLog("[CircleButtonCell constraints added]")
    }
    
}

//
//  EmbeddedUsersController.swift
//  Cards
//
//  Created by Nikita on 18.08.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit
import IGListKit

final class EmbeddedUsersController: ListSectionController {
    
    private var user:UserItem?
    
    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        let height = collectionContext?.containerSize.height ?? 0
        return CGSize(width: 80.0, height: height)
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell = collectionContext?.dequeueReusableCell(of: UserCell.self, for: self, at: index) as? UserCell else {
            fatalError("cell musb be 'CircleButtonCell'")
        }
        if let user = user {
            cell.text = user.title
            cell.image = user.image
        }
        return cell
    }
    
    override func didUpdate(to object: Any) {
        user = object as? UserItem
    }
}

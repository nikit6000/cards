//
//  CircleButtonItem.swift
//  Cards
//
//  Created by Nikita on 18.08.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit
import IGListKit

final class CircleButtonItem: NSObject {
    let emoji: String
    let title: String
    
    init(emoji:String, title:String) {
        self.emoji = emoji
        self.title = title
    }
}

extension CircleButtonItem: ListDiffable {
    func diffIdentifier() -> NSObjectProtocol {
        return (title + emoji) as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? CircleButtonItem else { return false }
        return emoji == object.emoji && title == object.title
    }
}

//
//  CircleButtonsItem.swift
//  Cards
//
//  Created by Nikita on 18.08.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import Foundation
import IGListKit

final class CircleButtonsItem: ListDiffable {
    let items:[CircleButtonItem]
    
    init(items: [CircleButtonItem]) {
        self.items = items
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return items as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? CircleButtonsItem else { return false }
        if items.count != object.items.count {
            return false
        }
        for i in 0..<items.count {
            let item = items[i]
            if !item.isEqual(toDiffableObject: object.items[i]) {
                return false
            }
        }
        return true
    }
}

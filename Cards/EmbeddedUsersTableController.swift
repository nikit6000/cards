//
//  EmbeddedUsersTableController.swift
//  Cards
//
//  Created by Nikita on 20.08.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit
import IGListKit

final class EmbeddedUsersTableController: ListSectionController {
    
    private var user:UserItem?
    
    override init() {
        super.init()
        //self.inset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        let width = collectionContext?.containerSize.width ?? 0
        return CGSize(width: width, height: 60.0)
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell = collectionContext?.dequeueReusableCell(of: UserExtendedCell.self, for: self, at: index) as? UserExtendedCell else {
            fatalError("cell musb be 'CircleButtonCell'")
        }
        if let user = user {
            cell.text = user.title
            cell.image = user.image
            cell.color = .white
            cell.userId = user.userId
            cell.subtitle = user.subtitle
            cell.subtextColor = .lightGray
            cell.isUserAdded = user.isAdded
        }
        return cell
    }
    
    override func didUpdate(to object: Any) {
        user = object as? UserItem
    }
}


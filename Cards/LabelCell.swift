//
//  LabelCell.swift
//  Cards
//
//  Created by Nikita on 18.08.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit
import IGListKit

final class LabelCell: UICollectionViewCell {
    
    lazy private var label: UILabel = {
        let view = UILabel()
        view.backgroundColor = .clear
        view.textColor = .white
        view.font = .systemFont(ofSize: 12)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textAlignment = .center
        self.contentView.addSubview(view)
        return view
    }()
    
    public var text:String? {
        get{
            return label.text
        }
        set(newText){
            label.text = newText
        }
    }
    
    public var textColot:UIColor {
        get{
            return label.textColor
        }
        set(newColor){
            label.textColor = newColor
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: setting up AutoLayout
    
    private func setupConstraints() {
        let leading = NSLayoutConstraint(item: label, attribute: .leading, relatedBy: .equal, toItem: contentView, attribute: .leading, multiplier: 1.0, constant: 0.0)
        let trailing = NSLayoutConstraint(item: contentView, attribute: .trailing, relatedBy: .equal, toItem: label, attribute: .trailing, multiplier: 1.0, constant: 0.0)
        let centerY = NSLayoutConstraint(item: label, attribute: .centerY, relatedBy: .equal, toItem: contentView, attribute: .centerY, multiplier: 1.0, constant: 0.0)
        
        contentView.addConstraints([leading, trailing, centerY])
    }
    
}


//
//  UsersTableController.swift
//  Cards
//
//  Created by Nikita on 20.08.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit
import IGListKit

final class UsersTableController: ListSectionController, ListAdapterDataSource {
    private var users: ArrayItem<ArrayItem<UserItem>>?
    
    /*lazy var adapter: ListAdapter = {
        let adapter = ListAdapter(updater: ListAdapterUpdater(), viewController: self.viewController)
        adapter.dataSource = self
        return adapter
    }()*/
    
    var adapters:[ListAdapter] = []
    
    override func sizeForItem(at index: Int) -> CGSize {
        /*var maximum:Int = 0
        if let users = users {
            maximum = users.items[0].items.count
            for item in users.items {
                if item.items.count < maximum {
                    maximum = item.items.count
                }
            }
        }
        return CGSize(width: collectionContext!.containerSize.width, height: 60.0 * CGFloat(maximum))*/
        NSLog("[UsersTableController: height for '%d' is %f]", index, 60.0 * CGFloat(users?.items[index].items.count ?? 0))
        return CGSize(width: collectionContext!.containerSize.width, height: 60.0 * CGFloat(users?.items[index].items.count ?? 0))
    }
    
    override func numberOfItems() -> Int {
        return users?.items.count ?? 0
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell = collectionContext?.dequeueReusableCell(of: EmbeddedTableCell.self, for: self, at: index) as? EmbeddedTableCell else {
            fatalError("Cell must be 'EmbeddedCollectionCell'")
        }
        adapters[index].collectionView = cell.collectionView
        return cell
    }
    
    override func didUpdate(to object: Any) {
        users = object as? ArrayItem<ArrayItem<UserItem>>
        if users != nil {
            adapters.removeAll()
            for _ in 0..<users!.items.count {
                let adapter = ListAdapter(updater: ListAdapterUpdater(), viewController: self.viewController)
                adapter.dataSource = self
                adapters.append(adapter)
            }
        }
    }
    
    // MARK: DataSource
    
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        guard let users = users else { return [] }
        guard let index = adapters.index(of: listAdapter) else {
            return []
        }
        NSLog("[UsersTableController sending objects for index: %d]", index)
        return users.items[index].items
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        return EmbeddedUsersTableController()
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        return nil
    }

}

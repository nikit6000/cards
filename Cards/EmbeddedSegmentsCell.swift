//
//  EmbeddedSegmentsCell.swift
//  Cards
//
//  Created by Nikita on 19.08.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit

final class EmbeddedSegmentsCell: UICollectionViewCell {
    
    lazy private var segmentView: SegmentView = {
        let view = SegmentView()
        view.showsVerticalScrollIndicator = false
        view.showsVerticalScrollIndicator = false
        view.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(view)
        return view
    }()
    
    public var segments:[String]{
        get{
            return segmentView.segments
        }
        set(newSegments){
            segmentView.segments = newSegments
        }
    }
    
    public var delegate: SegmentViewDelegate? {
        get{
            return segmentView.sDelegate
        }
        set{
            segmentView.sDelegate = newValue
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: setting up AutoLayout
    
    private func setupConstraints(){
        let topConstraint = NSLayoutConstraint(item: segmentView,
                                               attribute: .top,
                                               relatedBy: .equal,
                                               toItem: contentView,
                                               attribute: .top,
                                               multiplier: 1.0,
                                               constant: 0.0)
        
        let leadingConstraint = NSLayoutConstraint(item: segmentView,
                                                   attribute: .leading,
                                                   relatedBy: .equal,
                                                   toItem: contentView,
                                                   attribute: .leading,
                                                   multiplier: 1.0,
                                                   constant: 0.0)
        
        let trailingConstraint = NSLayoutConstraint(item: contentView,
                                                    attribute: .trailing,
                                                    relatedBy: .equal,
                                                    toItem: segmentView,
                                                    attribute: .trailing,
                                                    multiplier: 1.0,
                                                    constant: 0.0)
        
        let bottomConstraint = NSLayoutConstraint(item: contentView,
                                                  attribute: .bottom,
                                                  relatedBy: .equal,
                                                  toItem: segmentView,
                                                  attribute: .bottom,
                                                  multiplier: 1.0,
                                                  constant: 0.0)
        
        contentView.addConstraints([topConstraint, leadingConstraint,trailingConstraint, bottomConstraint])
        NSLog("[EmbeddedCollectionCell constraints added]")
    }
    
    
    
}

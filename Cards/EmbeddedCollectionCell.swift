//
//  EmbeddedCollectionCell.swift
//  Cards
//
//  Created by Nikita on 18.08.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit
import IGListKit

final class EmbeddedCollectionCell: UICollectionViewCell {
    
    lazy var collectionView: UICollectionView = {
        let layout = TopAlignedCollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.backgroundColor = .clear
        view.alwaysBounceVertical = false
        view.alwaysBounceHorizontal = true
        view.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(view)
        return view
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: setting up AutoLayout
    
    private func setupConstraints(){
        let topConstraint = NSLayoutConstraint(item: collectionView,
                                               attribute: .top,
                                               relatedBy: .equal,
                                               toItem: contentView,
                                               attribute: .top,
                                               multiplier: 1.0,
                                               constant: 0.0)
        
        let leadingConstraint = NSLayoutConstraint(item: collectionView,
                                                   attribute: .leading,
                                                   relatedBy: .equal,
                                                   toItem: contentView,
                                                   attribute: .leading,
                                                   multiplier: 1.0,
                                                   constant: 0.0)
        
        let trailingConstraint = NSLayoutConstraint(item: contentView,
                                                    attribute: .trailing,
                                                    relatedBy: .equal,
                                                    toItem: collectionView,
                                                    attribute: .trailing,
                                                    multiplier: 1.0,
                                                    constant: 0.0)
        
        let bottomConstraint = NSLayoutConstraint(item: contentView,
                                                  attribute: .bottom,
                                                  relatedBy: .equal,
                                                  toItem: collectionView,
                                                  attribute: .bottom,
                                                  multiplier: 1.0,
                                                  constant: 0.0)
        
        contentView.addConstraints([topConstraint, leadingConstraint,trailingConstraint, bottomConstraint])
        NSLog("[EmbeddedCollectionCell constraints added]")
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        setNeedsLayout()
        layoutIfNeeded()
        let size = contentView.systemLayoutSizeFitting(layoutAttributes.size)
        var newFrame = layoutAttributes.frame
        // note: don't change the width
        newFrame.size.height = ceil(size.height)
        layoutAttributes.frame = newFrame
        return layoutAttributes
    }
}

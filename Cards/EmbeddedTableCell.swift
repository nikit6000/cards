//
//  EmbeddedTableCell.swift
//  Cards
//
//  Created by Nikita on 19.08.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit
import IGListKit

final class EmbeddedTableCell: UICollectionViewCell {
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()//ListCollectionViewLayout(stickyHeaders: false, scrollDirection: .vertical, topContentInset: 0.0, stretchToEdge: true)
        layout.scrollDirection = .vertical
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.backgroundColor = .clear
        view.alwaysBounceVertical = false
        view.alwaysBounceHorizontal = false
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true
        view.layer.cornerRadius = 8.0
        self.contentView.addSubview(view)
        return view
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: setting up AutoLayout
    
    private func setupConstraints(){
        let topConstraint = NSLayoutConstraint(item: collectionView,
                                               attribute: .top,
                                               relatedBy: .equal,
                                               toItem: contentView,
                                               attribute: .top,
                                               multiplier: 1.0,
                                               constant: 0.0)
        
        let leadingConstraint = NSLayoutConstraint(item: collectionView,
                                                   attribute: .leading,
                                                   relatedBy: .equal,
                                                   toItem: contentView,
                                                   attribute: .leading,
                                                   multiplier: 1.0,
                                                   constant: 10.0)
        
        let trailingConstraint = NSLayoutConstraint(item: contentView,
                                                    attribute: .trailing,
                                                    relatedBy: .equal,
                                                    toItem: collectionView,
                                                    attribute: .trailing,
                                                    multiplier: 1.0,
                                                    constant: 10.0)
        
        let bottomConstraint = NSLayoutConstraint(item: contentView,
                                                  attribute: .bottom,
                                                  relatedBy: .equal,
                                                  toItem: collectionView,
                                                  attribute: .bottom,
                                                  multiplier: 1.0,
                                                  constant: 0.0)
        
        contentView.addConstraints([topConstraint, leadingConstraint,trailingConstraint, bottomConstraint])
        NSLog("[EmbeddedTableCell constraints added]")
    }
    
   
}

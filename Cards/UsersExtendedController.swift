//
//  UsersExtendedController.swift
//  Cards
//
//  Created by Nikita on 20.08.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit
import IGListKit

final class UsersExtendedController: ListSectionController, ListAdapterDataSource {
    fileprivate var currentPage:Int = 0
    fileprivate var users: ArrayItem<ArrayItem<UserItem>>?
    fileprivate var previousOffset:CGPoint = .zero
    fileprivate var maxHeight:CGFloat = 0.0
    lazy var adapter: ListAdapter = {
        let adapter = ListAdapter(updater: ListAdapterUpdater(), viewController: self.viewController)
        adapter.dataSource = self
        adapter.scrollViewDelegate = self
        return adapter
    }()
    
    override func sizeForItem(at index: Int) -> CGSize {
        
        self.maxHeight = 60.0 * CGFloat(users?.items[currentPage].items.count ?? 0)
        //needsMaximum = false
        return CGSize(width: collectionContext!.containerSize.width, height: self.maxHeight)
    }
    
    override func numberOfItems() -> Int {
        return 1
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell = collectionContext?.dequeueReusableCell(of: EmbeddedCollectionCell.self, for: self, at: index) as? EmbeddedCollectionCell else {
            fatalError("Cell must be 'EmbeddedCollectionCell'")
        }
        adapter.collectionView = cell.collectionView
        cell.collectionView.isPagingEnabled = true
        return cell
    }
    
    override func didUpdate(to object: Any) {
        users = object as? ArrayItem<ArrayItem<UserItem>>
    }
    
    // MARK: DataSource
    
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        guard let users = users else { return [] }
        return [users]
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        return UsersTableController()
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        return nil
    }
    
    fileprivate func maximum() -> CGFloat {
        var maximum:CGFloat = 0
        if let users = users {
            for item in users.items {
                if CGFloat(item.items.count) > maximum {
                    maximum = CGFloat(item.items.count)
                }
            }
        }
        return maximum
    }
}

extension UsersExtendedController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard let collectionView = scrollView as? UICollectionView else { return }
        //guard let users = users else { return }
        let isLeftDirection = previousOffset.x > collectionView.contentOffset.x
        let width = collectionContext!.containerSize.width
        let page = collectionView.contentOffset.x / width
        if fmodf(Float(collectionView.contentOffset.x), Float(width)) == 0 {
            if self.currentPage != Int(page) {
                self.currentPage = Int(page)
                print("Page", self.currentPage, "direction:", isLeftDirection ? "left" : "right")
                //collectionContext!.scroll(to: self, at: 0, scrollPosition: .top, animated: true)
                //needsMaximum = true
                collectionContext?.invalidateLayout(for: self, completion: nil)
            }
        }
        
        /*let deltaPage = (isLeftDirection ? -1 : 1) + self.currentPage
        
        if deltaPage >= 0 && deltaPage < users.items.count {
            let height = CGFloat(users.items[deltaPage].items.count) * 60.0
            let delta = self.maximum() - height
            let progress = fabsf(Float(page - CGFloat(currentPage)))
            print(-delta * CGFloat(progress))
        }*/
        
        previousOffset = collectionView.contentOffset
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
       
        
    }
}

//
//  UserUtil.swift
//  Cards
//
//  Created by Nikita on 18.08.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit

class UserUtil {
    public static let instance = UserUtil()
    
    let userNames:[[String]] = [      ["Anastasiya","girl"],
                                      ["Lizaveta","girl"],
                                      ["Mariya","girl"],
                                      ["Katya","girl"],
                                      ["Ludmila","girl"],
                                      ["Svetlana","girl"],
                                      ["Kseniya","girl"],
                                      ["Alena","girl"],
                                      ["Anna","girl"],
                                      ["Adelya","girl"],
                                      ["Nikita","boy"],
                                      ["Maxim","boy"],
                                      ["Kair","boy"],
                                      ["Ilya","boy"],
                                      ["Andrey","boy"],
                                      ["Dmitry","boy"],
                                      ["Konstantin","boy"],
                                      ["Vyacheslav","boy"],
                                      ["Evgeny","boy"],
                                      ["Valery","boy"],
                                      ["Alexander","boy"],
                                      ["Alexey","boy"],
                                      ["Sergey","boy"]]
    
    private var subtitles:[String] = ["✅ There are common friends",
                                      "🙄 Wow!",
                                      "👊🏻 Brother",
                                      "👀 Nice joke"]
    
    private var usersItem: UsersItem?
    
    private var extendedItems: UsersItem?
    
    public var users:UsersItem {
        get{
            if usersItem == nil {
                return generate(count: 25)
            } else {
                return usersItem!
            }
        }
    }
    
    public var extendedUsers:UsersItem {
        get{
            if extendedItems == nil {
                return generate(count: 25)
            } else {
                return extendedItems!
            }
        }
    }
    
    public func generate(count: Int, extended: Bool = false) -> UsersItem {
        var items:[UserItem] = []
        for i in 0..<count {
            let imageId = arc4random_uniform(25)
            let namesId = arc4random_uniform(UInt32(userNames.count))
            let title = userNames[Int(namesId)][0]
            let gender = userNames[Int(namesId)][1]
            let imageName = "\(gender)-\(imageId)"
            
            let subtitleRandom = arc4random_uniform(100)
            
            let needsSubtitle = subtitleRandom > 30 && subtitleRandom < 80
            let subtitle = needsSubtitle ? subtitles[Int(arc4random_uniform(4))] : ""
            let user = UserItem(userId: i, image: UIImage(named: imageName) ?? UIImage(named: "\(gender)-0")!, title: title, subtitle: subtitle, isAdded: false)
            items.append(user)
        }
        if extended {
            extendedItems = UsersItem(items: items, extended: extended)
            return extendedItems!
        } else {
            usersItem = UsersItem(items: items, extended: extended)
            return usersItem!
        }
    }
}

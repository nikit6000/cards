//
//  SwitchController.swift
//  Cards
//
//  Created by Nikita on 18.08.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import Foundation
import IGListKit

final class SegmentController: ListSectionController{
    
    private var segments: SegmentsItem?
    public var delegate: SegmentViewDelegate?
    
    init(delegate: SegmentViewDelegate?) {
        super.init()
        self.delegate = delegate
        self.inset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
    }
    
    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        return CGSize(width: collectionContext!.containerSize.width, height: 30.0)
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell = collectionContext?.dequeueReusableCell(of: EmbeddedSegmentsCell.self, for: self, at: index) as? EmbeddedSegmentsCell else {
            fatalError("Cell must be 'EmbeddedCollectionCell'")
        }
        cell.segments = segments?.items ?? []
        cell.delegate = delegate
        return cell
    }
    
    override func didUpdate(to object: Any) {
        segments = object as? SegmentsItem
    }
}







//
//  UserCell.swift
//  Cards
//
//  Created by Nikita on 18.08.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit

final class UserCell: UICollectionViewCell {
    
    lazy private var imageView: UIImageView = {
        let view = UIImageView()
        view.clipsToBounds = true
        view.contentMode = .scaleToFill
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.borderColor = UIColor.black.withAlphaComponent(0.3).cgColor
        view.layer.borderWidth = 0.5
        self.contentView.addSubview(view)
        return view
    }()
    
    lazy private var titleLabel: UILabel = {
        let view = UILabel()
        view.backgroundColor = .clear
        view.textAlignment = .center
        view.textColor = .black
        view.font = .systemFont(ofSize: 10)
        view.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(view)
        return view
    }()
    
    public var image:UIImage? {
        get{
            return imageView.image
        }
        set(newImage){
            imageView.image = newImage
        }
    }
    
    public var text: String? {
        get{
            return titleLabel.text
        }
        set(newText){
            titleLabel.text = newText
        }
    }
    
    public var textColor:UIColor {
        get{
            return titleLabel.textColor
        }
        set(newColor){
            titleLabel.textColor = newColor
        }
    }
    
    public var color:UIColor? {
        get{
            return contentView.backgroundColor
        }
        set(newColor){
            contentView.backgroundColor = newColor
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.contentView.clipsToBounds = true
        self.contentView.layer.cornerRadius = 8.0
        self.contentView.backgroundColor = .white
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imageView.layoutIfNeeded()
        imageView.layer.cornerRadius = imageView.frame.height / 2.0
    }
    
    // MARK setting up AutoLayout
    
    func setupConstraints() {
        do{
            let leading = NSLayoutConstraint(item: titleLabel, attribute: .leading, relatedBy: .equal, toItem: contentView, attribute: .leading, multiplier: 1.0, constant: 0.0)
            let trailing = NSLayoutConstraint(item: contentView, attribute: .trailing, relatedBy: .equal, toItem: titleLabel, attribute: .trailing, multiplier: 1.0, constant: 0.0)
            let bottom = NSLayoutConstraint(item: contentView, attribute: .bottom, relatedBy: .equal, toItem: titleLabel, attribute: .bottom, multiplier: 1.0, constant: 8.0)
            contentView.addConstraints([leading, trailing, bottom])
        }
        do{
            let top = NSLayoutConstraint(item: imageView, attribute: .top, relatedBy: .equal, toItem: contentView, attribute: .top, multiplier: 1.0, constant: 10.0)
            let leading = NSLayoutConstraint(item: imageView, attribute: .leading, relatedBy: .equal, toItem: contentView, attribute: .leading, multiplier: 1.0, constant: 16.0)
            let trailing = NSLayoutConstraint(item: contentView, attribute: .trailing, relatedBy: .equal, toItem: imageView, attribute: .trailing, multiplier: 1.0, constant: 16.0)
            let height = NSLayoutConstraint(item: imageView, attribute: .height, relatedBy: .equal, toItem: imageView, attribute: .width, multiplier: 1.0, constant: 0.0)
            contentView.addConstraints([top, leading, trailing])
            imageView.addConstraint(height)
        }
        NSLog("[UserCell constraints added]")
    }
    
    
}

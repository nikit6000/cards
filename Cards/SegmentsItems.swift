//
//  SegmentsItems.swift
//  Cards
//
//  Created by Nikita on 19.08.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit
import IGListKit

final class SegmentsItem: ListDiffable {
    let items:[String]
    
    init(items: [String]) {
        self.items = items
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return items as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? SegmentsItem else { return false }
        if items.count != object.items.count {
            return false
        }
        for i in 0..<items.count {
            let item = items[i]
            if item != object.items[i] {
                return false
            }
        }
        return true
    }
}


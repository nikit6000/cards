//
//  ViewController.swift
//  Cards
//
//  Created by Nikita on 18.08.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit
import IGListKit


class ViewController: UIViewController {

    
    private var searchLeading: NSLayoutConstraint?
    private var searchTrailing: NSLayoutConstraint?
    
    lazy var adapter: ListAdapter = {
        return ListAdapter(updater: ListAdapterUpdater(), viewController: self)
    }();
    
    var data: [Any] = [
        CircleButtonsItem(items: [CircleButtonItem(emoji: "📍", title: "Nearly"),
                                  CircleButtonItem(emoji: "🔀", title: "Random"),
                                  CircleButtonItem(emoji: "👫", title: "Friends of friends")]),
        "FAST ACCESS",
        UserUtil.instance.users,
        SegmentsItem(items: ["Chats","New friends"]),
        ArrayItem<ArrayItem<UserItem>>(items: [ ArrayItem<UserItem>(items: UserUtil.instance.generate(count: 10, extended: true).items),
                                                ArrayItem<UserItem>(items: UserUtil.instance.generate(count: 15, extended: true).items)])
        
    ]
    
    lazy var collectionView:UICollectionView = {
        let view = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        view.backgroundColor = .clear
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentInset = UIEdgeInsets(top: 60.0, left: 0.0, bottom: 10.0, right: 0.0)
        self.view.addSubview(view)
        return view
    }()
    
    lazy var navigationBar: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .black
        self.view.addSubview(view)
        return view
    }()
    
    lazy var searchButton: UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setImage(UIImage(named: "icon_search"), for: .normal)
        view.imageView!.contentMode = .scaleAspectFit
        view.addTarget(self, action: #selector(self.searchAction), for: .touchUpInside)
        self.navigationBar.addSubview(view)
        return view
    }()
    
    lazy var searchField: UITextField = {
        let view = UITextField()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.attributedPlaceholder = NSAttributedString(string: "Search", attributes: [NSForegroundColorAttributeName: UIColor.white.withAlphaComponent(0.7)])
        view.borderStyle = .none
        view.backgroundColor = .clear
        view.textColor = .white
        self.navigationBar.addSubview(view)
        return view
    }()
    
    lazy var dismissButton: UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setImage(UIImage(named: "icon_close"), for: .normal)
        view.imageView!.contentMode = .scaleAspectFit
        view.addTarget(self, action: #selector(self.dismissAction), for: .touchUpInside)
        self.navigationBar.addSubview(view)
        return view
    }()
    
    lazy var hairlineView: HairlineView = {
        let view = HairlineView()
        view.backgroundColor = .clear
        view.translatesAutoresizingMaskIntoConstraints = false
        self.navigationBar.addSubview(view)
        return view
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .black
        adapter.collectionView = collectionView
        adapter.dataSource = self
        setupConstraints()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setupConstraints(){
        // collectionView
        NSLayoutConstraint(item: collectionView, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1.0, constant: 0.0).isActive = true
        NSLayoutConstraint(item: collectionView, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1.0, constant: 0.0).isActive = true
        NSLayoutConstraint(item: view, attribute: .trailing, relatedBy: .equal, toItem: collectionView, attribute: .trailing, multiplier: 1.0, constant: 0.0).isActive = true
        NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: collectionView, attribute: .bottom, multiplier: 1.0, constant: 0.0).isActive = true
        
        // navigationBar
        NSLayoutConstraint(item: navigationBar, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1.0, constant: 0.0).isActive = true
        NSLayoutConstraint(item: navigationBar, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1.0, constant: 0.0).isActive = true
        NSLayoutConstraint(item: view, attribute: .trailing, relatedBy: .equal, toItem: navigationBar, attribute: .trailing, multiplier: 1.0, constant: 0.0).isActive = true
        NSLayoutConstraint(item: navigationBar, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 60.0).isActive = true
        
        // searchIcon
        
        let searchIconSize:CGFloat = 30.0
        
        NSLayoutConstraint(item: searchButton, attribute: .width, relatedBy: .equal, toItem: searchButton, attribute: .height, multiplier: 1.0, constant: 0.0).isActive = true
        NSLayoutConstraint(item: searchButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: searchIconSize).isActive = true
        searchLeading = NSLayoutConstraint(item: searchButton, attribute: .leading, relatedBy: .equal, toItem: navigationBar, attribute: .leading, multiplier: 1.0, constant: 8.0)
        searchTrailing = NSLayoutConstraint(item: navigationBar, attribute: .trailing, relatedBy: .equal, toItem: searchButton, attribute: .trailing, multiplier: 1.0, constant: 8.0)
        searchTrailing?.isActive = true
        NSLayoutConstraint(item: searchButton, attribute: .centerY, relatedBy: .equal, toItem: navigationBar, attribute: .centerY, multiplier: 1.0, constant: 0.0).isActive = true
        
        // searchField
        
        NSLayoutConstraint(item: searchField, attribute: .width, relatedBy: .equal, toItem: navigationBar, attribute: .width, multiplier: 1.0, constant: -(2 * searchIconSize) - 32).isActive = true
        NSLayoutConstraint(item: searchField, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 30).isActive = true
        NSLayoutConstraint(item: searchField, attribute: .leading, relatedBy: .equal, toItem: searchButton, attribute: .trailing, multiplier: 1.0, constant: 8.0).isActive = true
        NSLayoutConstraint(item: searchField, attribute: .centerY, relatedBy: .equal, toItem: navigationBar, attribute: .centerY, multiplier: 1.0, constant: 0.0).isActive = true
        
        // dismissButton
        NSLayoutConstraint(item: dismissButton, attribute: .width, relatedBy: .equal, toItem: dismissButton, attribute: .height, multiplier: 1.0, constant: 0.0).isActive = true
        NSLayoutConstraint(item: dismissButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: searchIconSize).isActive = true
        NSLayoutConstraint(item: dismissButton, attribute: .leading, relatedBy: .equal, toItem: searchField, attribute: .trailing, multiplier: 1.0, constant: 8.0).isActive = true
        NSLayoutConstraint(item: dismissButton, attribute: .centerY, relatedBy: .equal, toItem: navigationBar, attribute: .centerY, multiplier: 1.0, constant: 0.0).isActive = true
        
        // hairlineView
        NSLayoutConstraint(item: hairlineView, attribute: .leading, relatedBy: .equal, toItem: navigationBar, attribute: .leading, multiplier: 1.0, constant: 0.0).isActive = true
        NSLayoutConstraint(item: hairlineView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 1.0).isActive = true
        NSLayoutConstraint(item: navigationBar, attribute: .trailing, relatedBy: .equal, toItem: hairlineView, attribute: .trailing, multiplier: 1.0, constant: 0.0).isActive = true
        NSLayoutConstraint(item: navigationBar, attribute: .bottom, relatedBy: .equal, toItem: hairlineView, attribute: .bottom, multiplier: 1.0, constant: 0.0).isActive = true
        
        view.setNeedsLayout()
        view.layoutIfNeeded()
        NSLog("[ViewController constraints added]")
    }

    @objc private func searchAction(){
        searchButton.isEnabled = false
        searchTrailing?.isActive = false
        searchLeading?.isActive = true
        navigationBar.setNeedsLayout()
        UIView.animate(withDuration: 0.3, animations: {
            self.navigationBar.layoutIfNeeded()
        })
    }
    
    @objc private func dismissAction(){
        searchLeading?.isActive = false
        searchTrailing?.isActive = true
        navigationBar.setNeedsLayout()
        UIView.animate(withDuration: 0.3, animations: {
            self.navigationBar.layoutIfNeeded()
        }, completion: { _ in
            self.searchButton.isEnabled = true
        })
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }

}

// MARK: -

extension ViewController: ListAdapterDataSource {
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        return data as? [ListDiffable] ?? []
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        if object is String {
            return LabelController()
        } else if object is UsersItem {
            return UsersController()
        } else if object is SegmentsItem {
            return SegmentController(delegate: self)
        } else if object is ArrayItem<ArrayItem<UserItem>>{
            return UsersExtendedController()
        } else {
            return CircleButtonsController()
        }
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        return nil
    }
}

// MARK: SegmentViewDelegate

extension ViewController: SegmentViewDelegate {
    func segmentView(_ segmetVeiw: SegmentView, didChangeIndex index: Int) {
        /*NSLog("[ViewController selected segment: %d]", index)
        if index == 0 {
            data[4] = UserUtil.instance.generate(count: 10, extended: true)
        } else {
            data[4] = UserUtil.instance.generate(count: 5, extended: true)
        }
        if let controller = adapter.sectionController(forSection: 4) {
            controller.didUpdate(to: data[4])
            controller.collectionContext?.performBatch(animated: true, updates: { context in
                context.reload(controller)
            }, completion: nil)
        }*/
    }
}


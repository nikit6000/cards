//
//  CircleButtonsController.swift
//  Cards
//
//  Created by Nikita on 18.08.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import Foundation
import IGListKit

protocol CircleButtonsDelegate {
    func circleButtons(selected index: Int)
}

final class CircleButtonsController: ListSectionController, ListAdapterDataSource{
    
    private var buttons: CircleButtonsItem?
    
    lazy var adapter: ListAdapter = {
        let adapter = ListAdapter(updater: ListAdapterUpdater(), viewController: self.viewController)
        adapter.dataSource = self
        return adapter
    }()
    
    override func sizeForItem(at index: Int) -> CGSize {
        return CGSize(width: collectionContext!.containerSize.width, height: 100.0)
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell = collectionContext?.dequeueReusableCell(of: EmbeddedCollectionCell.self, for: self, at: index) as? EmbeddedCollectionCell else {
            fatalError("Cell must be 'EmbeddedCollectionCell'")
        }
        adapter.collectionView = cell.collectionView
        return cell
    }
    
    override func didUpdate(to object: Any) {
        buttons = object as? CircleButtonsItem
    }
    
    // MARK: DataSource
    
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        guard let buttons = buttons else { return [] }
        return buttons.items
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        return EmbeddedButtonsController()
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        return nil
    }
}

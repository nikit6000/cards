//
//  ArrayItem.swift
//  Cards
//
//  Created by Nikita on 20.08.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit
import IGListKit

class ArrayItem<T:ListDiffable>: ListDiffable {
    public var items:[T]
    
    init(items:[T]) {
        self.items = items
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return "\(items.count)\(NSStringFromClass(T.self))" as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard let array = object as? ArrayItem<T> else { return false }
        if items.count != array.items.count { return false }
        for i in 0..<items.count {
            if !items[i].isEqual(toDiffableObject: array.items[i]) {
                return false
            }
        }
        return true
    }
    
}


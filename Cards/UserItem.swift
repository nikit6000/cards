//
//  UserItem.swift
//  Cards
//
//  Created by Nikita on 18.08.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit
import IGListKit

final class UserItem: NSObject {
    
    let userId: Int
    let image: UIImage
    let title: String
    let subtitle: String
    var isAdded:Bool
    
    init(userId: Int, image: UIImage, title: String, subtitle: String, isAdded:Bool) {
        self.userId = userId
        self.image = image
        self.title = title
        self.subtitle = subtitle
        self.isAdded = isAdded
    }
}

extension UserItem: ListDiffable {
    func diffIdentifier() -> NSObjectProtocol {
        return userId as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? UserItem else { return false }
        return userId == object.userId
    }
}

//
//  SegmentView.swift
//  Cards
//
//  Created by Nikita on 19.08.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit

protocol SegmentViewDelegate {
    func segmentView(_ segmetVeiw: SegmentView, didChangeIndex index: Int)
}

final class SegmentView: UIScrollView {
    
    fileprivate var labels:[UILabel] = []
    fileprivate var currentIndex:Int = 0 {
        didSet{
            sDelegate?.segmentView(self, didChangeIndex: currentIndex)
        }
    }
    
    public var inxex: Int {
        return currentIndex
    }
    
    public var sDelegate: SegmentViewDelegate?
    
    public var segments:[String] = [] {
        didSet {
            for item in subviews {
                item.removeFromSuperview()
            }
            labels.removeAll()
            for segment in segments {
                pushLabel(text: segment)
            }
            setupConstraints()
            move(to: labels.first?.center ?? .zero)
            labels.first?.textColor = UIColor.white
            labels.first?.font = .systemFont(ofSize: 16)
            
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        delegate = self
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let inset = self.bounds.width / 2.0
        self.contentInset = UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
    }
    
    private func pushLabel(text: String){
        let view = UILabel()
        view.backgroundColor = .clear
        view.textAlignment = .center
        view.textColor = UIColor.white.withAlphaComponent(0.7)
        view.font = .systemFont(ofSize: 14)
        view.text = text
        view.sizeToFit()
        view.translatesAutoresizingMaskIntoConstraints = false
        labels.append(view)
    }
    
    func setupConstraints() {
        NSLayoutConstraint.deactivate(self.constraints)
        
        for (index, label) in labels.enumerated() {
            addSubview(label)
            if index == 0 {
                NSLayoutConstraint(item: label, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1.0, constant: 8.0).isActive = true
            } else  {
                if index == labels.count - 1 {
                    NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: label, attribute: .trailing, multiplier: 1.0, constant: 8.0).isActive = true
                }
                let previous = labels[index - 1]
                NSLayoutConstraint(item: label, attribute: .leading, relatedBy: .equal, toItem: previous, attribute: .trailing, multiplier: 1.0, constant: 8.0).isActive = true
            }
            
            NSLayoutConstraint(item: label, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1.0, constant: 0.0).isActive = true
        
        }
        
        setNeedsLayout()
        layoutIfNeeded()
    }
    
    fileprivate func move(to point: CGPoint) {
        self.setContentOffset(CGPoint(x: point.x - self.bounds.width / 2.0 , y: 0), animated: true)
    }
    
    fileprivate func getNearlest(point: CGPoint, centred: Bool = true) -> (UILabel, Int)? {
        guard let firstLabel = labels.first else { return nil }
        var nearlestLabel:UILabel = firstLabel
        var currentIndex = 0
        for (index, label) in labels.enumerated() {
            let offset = centred ? self.contentOffset.x : 0.0
            let closestDelta = abs(nearlestLabel.center.x - point.x - offset)
            let labelDelta = abs(label.center.x - point.x - offset)
            if (labelDelta < closestDelta){
                nearlestLabel = label
                currentIndex = index
            }
            label.textColor = UIColor.white.withAlphaComponent(0.7)
            label.font = .systemFont(ofSize: 14)
        }
        return (nearlestLabel, currentIndex)
    }
    
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else { return }
        
        let location = touch.location(in: self)
        
        if let nearlest = getNearlest(point: location, centred: false) {
            self.move(to: nearlest.0.center)
            nearlest.0.textColor = UIColor.white
            nearlest.0.font = .systemFont(ofSize: 16)
            self.currentIndex = nearlest.1
        }
        
    }
}

extension SegmentView:UIScrollViewDelegate{
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if decelerate { return }
        if let nearlest = getNearlest(point: CGPoint(x: self.bounds.width / 2.0, y: 0)) {
            self.move(to: nearlest.0.center)
            nearlest.0.textColor = UIColor.white
            nearlest.0.font = .systemFont(ofSize: 16)
            self.currentIndex = nearlest.1
        }
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let nearlest = getNearlest(point: CGPoint(x: self.bounds.width / 2.0, y: 0)) {
            self.move(to: nearlest.0.center)
            nearlest.0.textColor = UIColor.white
            nearlest.0.font = .systemFont(ofSize: 16)
            self.currentIndex = nearlest.1
        }
    }
    
}

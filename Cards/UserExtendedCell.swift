//
//  UserExtendedCell.swift
//  Cards
//
//  Created by Nikita on 19.08.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit

class UserExtendedCell: UICollectionViewCell {
    
    private var buttonWidth: NSLayoutConstraint?
    private let buttonColor = UIColor(displayP3Red: 82.0/255.0, green: 207.0/255.0, blue: 237/255.0, alpha: 1.0)
    private var titleOriginY: NSLayoutConstraint?
    
    lazy private var imageView: UIImageView = {
        let view = UIImageView()
        view.clipsToBounds = true
        view.contentMode = .scaleToFill
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.borderColor = UIColor.black.withAlphaComponent(0.3).cgColor
        view.layer.borderWidth = 0.5
        view.layer.cornerRadius = 25.0
        self.contentView.addSubview(view)
        return view
    }()
    
    lazy private var titleLabel: UILabel = {
        let view = UILabel()
        view.backgroundColor = .clear
        view.textAlignment = .left
        view.textColor = .black
        view.font = .boldSystemFont(ofSize: 14)
        view.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(view)
        return view
    }()
    
    lazy private var subtitleLabel: UILabel = {
        let view = UILabel()
        view.backgroundColor = .clear
        view.textAlignment = .left
        view.textColor = .black
        view.font = .systemFont(ofSize: 10)
        view.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(view)
        return view
    }();
    
    lazy private var button: UIButton = {
        let view = UIButton()
        view.backgroundColor = .clear
        view.layer.borderColor = self.buttonColor.cgColor
        view.layer.borderWidth = 1.0
        view.clipsToBounds = true
        view.layer.cornerRadius = 15.0
        view.setTitle("Add", for: .normal)
        view.setTitleColor(self.buttonColor, for: .normal)
        view.titleLabel!.font = .systemFont(ofSize: 11)
        view.setImage(UIImage(named: "icon_check"), for: .selected)
        view.imageView!.contentMode = .scaleAspectFit
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(self.buttonAction(_:)), for: .touchUpInside)
        self.contentView.addSubview(view)
        return view
    }()
    
    public var image:UIImage? {
        get{
            return imageView.image
        }
        set(newImage){
            imageView.image = newImage
        }
    }
    
    public var text: String? {
        get{
            return titleLabel.text
        }
        set(newText){
            titleLabel.text = newText
        }
    }
    
    public var textColor:UIColor {
        get{
            return titleLabel.textColor
        }
        set(newColor){
            titleLabel.textColor = newColor
        }
    }
    
    public var subtitle: String? {
        get{
            return subtitleLabel.text
        }
        set(newText){
            subtitleLabel.text = newText
            if newText == nil || newText!.isEmpty {
                self.titleOriginY?.constant = 0.0
            } else {
                self.titleOriginY?.constant = -10.0
            }
            layoutIfNeeded()
        }
    }
    
    public var subtextColor:UIColor {
        get{
            return subtitleLabel.textColor
        }
        set(newColor){
            subtitleLabel.textColor = newColor
        }
    }
    
    public var isUserAdded:Bool = false {
        didSet{
            let newButtonWidth:CGFloat
            if isUserAdded {
                newButtonWidth = 50.0
                button.backgroundColor = self.buttonColor
            } else {
                newButtonWidth = 80.0
                button.backgroundColor = .clear
            }
            self.buttonWidth?.constant = newButtonWidth
            UIView.animate(withDuration: 0.3, animations: {
                self.layoutIfNeeded()
            })
            
        }
    }
    
    public var userId:Int = -1
    
    public var color:UIColor? {
        get{
            return contentView.backgroundColor
        }
        set(newColor){
            contentView.backgroundColor = newColor
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupConstraints(){
        NSLayoutConstraint.deactivate(self.constraints)
        
        // imageView
        NSLayoutConstraint(item: imageView, attribute: .width, relatedBy: .equal, toItem: imageView, attribute: .height, multiplier: 1.0, constant: 0.0).isActive = true
        NSLayoutConstraint(item: imageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50.0).isActive = true
        NSLayoutConstraint(item: imageView, attribute: .leading, relatedBy: .equal, toItem: contentView, attribute: .leading, multiplier: 1.0, constant: 8.0).isActive = true
        NSLayoutConstraint(item: imageView, attribute: .centerY, relatedBy: .equal, toItem: contentView, attribute: .centerY, multiplier: 1.0, constant: 0.0).isActive = true
        
        // button
        buttonWidth = NSLayoutConstraint(item: button, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 80.0)
        buttonWidth!.isActive = true
        NSLayoutConstraint(item: button, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 30.0).isActive = true
        NSLayoutConstraint(item: contentView, attribute: .trailing, relatedBy: .equal, toItem: button, attribute: .trailing, multiplier: 1.0, constant: 8.0).isActive = true
        NSLayoutConstraint(item: button, attribute: .centerY, relatedBy: .equal, toItem: contentView, attribute: .centerY, multiplier: 1.0, constant: 0.0).isActive = true
        
        // titleLabel
        NSLayoutConstraint(item: titleLabel, attribute: .leading, relatedBy: .equal, toItem: imageView, attribute: .trailing, multiplier: 1.0, constant: 8.0).isActive = true
        NSLayoutConstraint(item: button, attribute: .leading, relatedBy: .equal, toItem: titleLabel, attribute: .trailing, multiplier: 1.0, constant: 8.0).isActive = true
        titleOriginY = NSLayoutConstraint(item: titleLabel, attribute: .centerY, relatedBy: .equal, toItem: contentView, attribute: .centerY, multiplier: 1.0, constant: -10.0)
        titleOriginY!.isActive = true
        
        // subtitleLabel
        NSLayoutConstraint(item: subtitleLabel, attribute: .leading, relatedBy: .equal, toItem: imageView, attribute: .trailing, multiplier: 1.0, constant: 8.0).isActive = true
        NSLayoutConstraint(item: button, attribute: .leading, relatedBy: .equal, toItem: subtitleLabel, attribute: .trailing, multiplier: 1.0, constant: 8.0).isActive = true
        NSLayoutConstraint(item: subtitleLabel, attribute: .centerY, relatedBy: .equal, toItem: contentView, attribute: .centerY, multiplier: 1.0, constant: 10.0).isActive = true
        
        
        setNeedsLayout()
        layoutIfNeeded()
    }
    
    @objc private func buttonAction(_ sender: Any?){
        let extended = UserUtil.instance.extendedUsers.items
        if userId < extended.count {
            extended[userId].isAdded = !extended[userId].isAdded
            button.isSelected = extended[userId].isAdded
            self.isUserAdded = extended[userId].isAdded
        }
    }
    
    
}

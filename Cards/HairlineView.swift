//
//  HairlineView.swift
//  Cards
//
//  Created by Nikita on 19.08.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit

class HairlineView: UIView {
    var lineColor:UIColor = UIColor.white.withAlphaComponent(0.7)
    
    override func awakeFromNib() {
        self.backgroundColor = UIColor.clear
    }
    
    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        
        context.setLineWidth(1/UIScreen.main.scale)
        context.translateBy(x: 0.0, y: 0.5)
        
        
        context.setStrokeColor(lineColor.cgColor)
        
        context.move(to: CGPoint(x: 0, y: self.bounds.size.height / 2.0))
        context.addLine(to: CGPoint(x: self.bounds.size.width, y:self.bounds.size.height / 2.0))
        context.strokePath()
    }
}

//
//  EmbeddedSectionController.swift
//  Cards
//
//  Created by Nikita on 18.08.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit
import IGListKit

final class EmbeddedButtonsController: ListSectionController {
    
    private var button:CircleButtonItem?
    
    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        let height = collectionContext?.containerSize.height ?? 0
        let width = collectionContext?.containerSize.width ?? 0
        return CGSize(width: width / 3.0 - 20.0, height: height)
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell = collectionContext?.dequeueReusableCell(of: CircleButtonCell.self, for: self, at: index) as? CircleButtonCell else {
            fatalError("cell musb be 'CircleButtonCell'")
        }
        if let button = button {
            cell.title = button.title
            cell.emoji = button.emoji
        }
        return cell
    }
    
    override func didUpdate(to object: Any) {
        button = object as? CircleButtonItem
    }
}
